<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

/*Route::get('/admin', function(){
    return view('admin.pages.tab-page', 
               ['tabBtn1' => 'Украинский',
                'tabBtn2' => 'Русский'
]);
});
*/
/* Route::get('/admin', function(){
    return view('admin.pages.search-table-paging-2');
}); */

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
/* Route::get('/', function(){
    return view('admin.pages.banners');
});
 */

Auth::routes();
    //
    Route::get('/admin', [App\Http\Controllers\AdminController::class, 'index'])->name('admin');
   
Route::group(['middleware' => ['role:admin']], function () {
    //
    Route::get('/admin', [App\Http\Controllers\AdminController::class, 'index'])->name('admin');
    Route::get('/admin/banners', 'AdminBannerController@execute')->name('banners');
});
   