<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Contact Form Template | PrepBootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/font-awesome.min.css') }}" />

</head>
<body>
    <div class="row mb-3">
        <div class="col-sm-2">
            <div class="col-form-label">SEO блок</div>
        </div>
        <div class="col-sm-10">
            <form>
                <div class="row mb-3">
                    <label for="inputSeoUrl" class="col-sm-2 col-form-label">URL</label>
                     <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputSeoUrl" name="seoUrl">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputTitle" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputTitle"  name="seoTitle">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputKeywords" class="col-sm-2 col-form-label">Keywords</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputKeywords"  name="seoKeywords">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputDescription" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" aria-label="inputDescription"  name="seoDescription"></textarea>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
</body>
</html>