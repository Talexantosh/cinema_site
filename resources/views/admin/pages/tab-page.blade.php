
<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Модальное окно на чистом CSS</title>
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/modal-window.css') }}" >
</head>

<body>
  <div class="tabs">
    <input type="radio" name="tab-btn" id="tab-btn-1" value="" checked>
    <label for="tab-btn-1">{{$tabBtn1}}</label>
    <input type="radio" name="tab-btn" id="tab-btn-2" value="">
    <label for="tab-btn-2">{{$tabBtn2}}</label>

    <div id="content-1">
      Содержимое 1...
    </div>
    <div id="content-2">
      Содержимое 2...
    </div>
  </div>

</body>

</html>
						