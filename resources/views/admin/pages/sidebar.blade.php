<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Sidebars · Bootstrap v5.0</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sidebars/">

    

    <!-- Bootstrap core CSS -->
<link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<meta name="theme-color" content="#7952b3">
    
    <!-- Custom styles for this template -->
    <link href="{{ asset('admin/css/sidebars.css') }}" rel="stylesheet">
  </head>
  <body>
<main>
  <div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 210px;">
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li class="nav-item heightless">
        <a href="#" class="nav-link active" aria-current="page">
          Статистика
        </a>
      </li>
      <li class="nav-item heightless">
        <a href="#" class="nav-link link-dark">
          Баннера/Слайдеры
        </a>
      </li>
      <li class="nav-item heightless">
        <a href="#" class="nav-link link-dark">
          Фильмы
        </a>
      </li>
      <li class="nav-item heightless">
        <a href="#" class="nav-link link-dark">
          Кинотеатры
        </a>
      </li>
      <li class="nav-item heightless">
        <a href="#" class="nav-link link-dark">
          Новости
        </a>
      </li>
      <li class="nav-item heightless">
        <a href="#" class="nav-link link-dark">
          Акции
        </a>
      </li>
      <li class="nav-item heightless">
        <a href="#" class="nav-link link-dark">
          Страницы
        </a>
      </li>
      <li class="nav-item heightless">
        <a href="#" class="nav-link link-dark">
          Пользователи
        </a>
      </li>
      <li class="nav-item heightless">
        <a href="#" class="nav-link link-dark">
          Рассылка
        </a>
      </li>
      <hr>
    </ul>
  </div>

  <div class="b-example-divider"></div>
  </main>


    <script src="{{ asset('admin/js/bootstrap.bundle.min.js')}}" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

      <script src="{{asset('admin/js/sidebars.js')}}"></script>
  

</body></html>