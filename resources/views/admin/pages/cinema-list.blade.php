<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Album example · Bootstrap v5.0</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    <!-- Bootstrap core CSS -->
<link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

  </head>
  <body>
    
<header>
  
</header>

<main>

  <section class="py-1 text-center container">
    <div class="row py-lg-1">
      <div class="col-lg-6 col-md-8 mx-auto">
        <h1 class="fw-light">Список кинотеатров</h1>
      </div>
    </div>
  </section>

  <div class="album py-5 bg-light">
    <div class="container">

      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4 g-4">
        <div class="col">
          <div class="card shadow-sm">
            <a class="text-dark text-decoration-none" href="#">
                <img src="{{ asset('admin/media/image/logo/logo.jpg')  }}" class="bd-placeholder-img card-img-top" alt="$altText" height="225" width="100%" /> 
                <div class="card-body">
                    <h5 class="card-text text-center">$cinemaName</h5>
                </div>
            </a>
          </div>
        </div>
        <div class="col">
          <div class="card shadow-sm">
            <a class="text-dark text-decoration-none" href="#">
                <img src="{{ asset('admin/media/image/logo/logo.jpg')  }}" class="bd-placeholder-img card-img-top" alt="$altText" height="225" width="100%" /> 
                <div class="card-body">
                    <h5 class="card-text text-center">$cinemaName</h5>
                </div>
            </a>
          </div>
        </div>
        <div class="col">
          <div class="card shadow-sm">
            <a class="text-dark text-decoration-none" href="#">
                <img src="{{ asset('admin/media/image/logo/logo.jpg')  }}" class="bd-placeholder-img card-img-top" alt="$altText" height="225" width="100%" /> 
                <div class="card-body">
                    <h5 class="card-text text-center">$cinemaName</h5>
                </div>
            </a>
          </div>
        </div>

        <div class="col">
          <div class="card shadow-sm">
            <a class="text-dark text-decoration-none" href="#">
                <div class="bd-placeholder-img card-img-top" style="height: 225px; width: 100%;"> 
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="bd-placeholder-img card-img-top svg-inline--fa fa-plus" style="margin-top: 62px" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="100"><path fill="currentColor" d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z"></path></svg>
                </div>
                <div class="card-body">
                    <h5 class="card-text text-center">Добавить</h5>
                </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

</main>

<footer class="text-muted py-5">

</footer>


    <script src="{{ asset('admin/js/bootstrap.bundle.min.js') }}" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

      
  

</body></html>