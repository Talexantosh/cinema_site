<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminBannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /* public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['role:admin|owner']);
    }
 */
    /**
     * Show page customization application banners.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function execute()
    {
        return view('admin.pages.banners');
    }
}
